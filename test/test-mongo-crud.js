//needed for the logging module
//global.__basedir = "../logs";

const request = require('request');
const expect = require('chai').expect;
const chai = require('chai');
const sinon = require('sinon');
const assert = require("assert");
const mocha = require("mocha");

const mongoApi = require('../app-modules/mongo-api');

describe('CRUD mongo API', function () {


    it('Should create document', function (done) {

        let req = {
            body: {
                firstName: "first",
                lastName: "last",
                jobDescription: "janitor"
            }
        };

        let res = {
            send: sinon.spy(),
            status: sinon.spy()
        };

       // mongoApi.createDocument(req, res);

        //sinon.assert.called(res.send);

        done();
    });

    it('Should get document by ID', function (done) {

        let req = {
            params: {
                id: "123"
            }
        };

        let res = {
            send: sinon.spy(),
            status: sinon.spy()
        };

        //mongoApi.getDocumentById(req, res);

        //sinon.assert.called(res.send);

        done();
    });


    it('Should get all documents', function (done) {

        let req = sinon.spy();

        let res = {
            send: sinon.spy(),
            status: sinon.spy()
        };

        //mongoApi.getAllDocuments(req, res);

        //sinon.assert.called(res.send);

        done();
    });

    it('Should  update document by ID', function (done) {

        let req = {
            body: {
                params: {
                    id: "123"
                }
            }
        };

        let res = {
            send: sinon.spy(),
            status: sinon.spy()
        };

        //mongoApi.updateDocument(req, res);

        //sinon.assert.called(res.send);

        done();
    });

    it('Should  delete document by ID', function (done) {
        let req = {
            params: {
                id: "123"
            }
        };

        let res = {
            send: sinon.spy(),
            status: sinon.spy()
        };

        //mongoApi.deleteDocument(req, res);
        //sinon.assert.called(res.send);

        done();
    });
});


