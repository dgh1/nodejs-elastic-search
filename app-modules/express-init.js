const productionMode =  process.env.PRODUCTION;
const logger = require('./logger');
const express = require('express');

const http = require('http');
const https = require('https');
const fs = require('fs');

const os = require("os");
const hostname = os.hostname();
const netInterfaces =  os.networkInterfaces( );

/**
 * Set up the express configuration
 */
const hostnameIp = '127.0.0.1';
const port = 80;
const exp = express();

if (productionMode === "true") {

    let httpsOptions = {
        key: fs.readFileSync('./ssl/private/server.key'),
        cert: fs.readFileSync('./ssl/certs/server.cert')
    };

    https.createServer(httpsOptions, exp).listen(port , () => {
        console.log(`- web application listening on https://localhost:${port}`);
        logger.info("- web application listening on https://localhost:" + port);
    });
}
else {
    http.createServer(exp).listen(port,  () => {
        console.log(`- web application listening on http://localhost:${port}`);
        logger.info("- web application listening on http://localhost:" + port);

        console.log("HOSTNAME: " + hostname );


        if (netInterfaces.eth0) {
            console.log("HOST IP: " +netInterfaces.eth0[0].address);
        }
        if (netInterfaces.wlp3s0) {
            console.log("HOST IP: " +netInterfaces.wlp3s0[0].address);
        }

        

    });
}






// config POST  to use json
exp.use(express.json());

// specifies directory to be used to serve up the web application
// static content files, such as, html, css, images  etc.

if (process.env.DEV_TEST &&  process.env.DEV_TEST === "true") {
    exp.use('/', express.static('dist/devtest'));
}
else {
    exp.use('/', express.static('dist/webapp'));
}


module.exports = {
    exp: exp
};

