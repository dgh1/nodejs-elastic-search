const logger = require('./logger');
const models = require('./mongoose-models');


/**
 * Create a newDocument
 *
 * @param req
 * @param res
 * @param data
 */
const createDocument = (req, res) => {
    logger.info("Mongoose - createDocument() ");

    let data = req.body;

    let entry = new models.personModel({
        firstName: data.firstName,
        lastName: data.lastName,
        jobDescription: data.jobDescription
    });

    let promise = new Promise(function (resolve, reject) {
        entry.save(function (err, result) {
            if (err !== null) {
                reject({
                    error: "failed to create document"
                });
            }
            resolve(result);
        });
    });

    promise.then(function (result) {
        res.status(200);
        res.send({
            status: "OK",
            document: result
        });
    }).catch((reason) => {
        logger.info(reason);
        res.status(500).send({
            message: "check nodejs logs"
        });
    });
};

/**
 * Update an  existing document by ID
 *
 * @param req
 * @param res
 */
const updateDocument = (req, res) => {
    logger.info("Mongoose - updateDocument() ");

    let data = req.body;

    let promise = new Promise(function (resolve, reject) {
        models.personModel.findOneAndUpdate({"_id": data.id}, data, function (err, result) {
            if (err !== null) {
                reject({
                    error: "failed to update document ID: " + data.id
                });
            }
            resolve(result);

        });
    });

    promise.then(function (/*result*/) {
        res.status(200);
        res.send({
            status: "OK"
        });

    }).catch((reason) => {
        logger.info(reason);
        res.status(500).send({
            message: "check nodejs logs"
        });
    });
};

/**
 * Delete a document by ID
 *
 * @param req
 * @param res
 * @param id
 */
const deleteDocument = (req, res) => {
    logger.info("Mongoose - deleteDocument() ");

    let params = JSON.parse(JSON.stringify(req.params));

    let promise = new Promise(function (resolve, reject) {
        models.personModel.findOneAndDelete({"_id": params.id}, function (err, result) {
            if (err !== null) {
                reject({
                    error: "failed to delete document ID: " + params.id
                });
            }
            resolve(result);

        });
    });

    promise.then(function (/*result*/) {
        res.status(200);
        res.send({
            status: "OK"
        });

    }).catch((reason) => {
        logger.info(reason);
        res.status(500).send({
            message: "check nodejs logs"
        });
    });
};

/**
 *  Return all documents
 *
 * @param req
 * @param res
 */
const getAllDocuments = (req, res) => {
    logger.info("Mongoose - getAllDocuments() ");

    let promise = new Promise(function (resolve, reject) {
        models.personModel.find(function (err, persons) {
            if (err !== null) {
                reject({
                    error: "failed to get all documents"
                });
            }
            resolve(persons);
        });
    });

    promise.then(function (persons) {
        let docs = [];

        for (let doc of persons) {
            docs.push({
                id: doc._id,
                firstName: doc.firstName,
                lastName: doc.lastName,
                jobDescription: doc.jobDescription,
            });
        }
        res.status(200);
        res.send(docs);
    }).catch((reason) => {
        logger.info(reason);
        res.status(500).send({
            message: "check nodejs logs"
        });
    });
};

/**
 * Get Document by ID
 *
 * @param req
 * @param res
 * @param id
 */
const getDocumentById = (req, res) => {
    logger.info("Mongoose - getDocumentById() ");

    let params = JSON.parse(JSON.stringify(req.params));

    let promise = new Promise(function (resolve, reject) {
        models.personModel.findById(params.id, (err, result) => {
            if (err !== null) {
                reject({
                    error: "failed to find document for ID " + params.id
                });
            }
            resolve(result);
        });
    });

    promise.then(function (result) {
        res.status(200);
        res.send({
            id: result._id,
            firstName: result.firstName,
            lastName: result.lastName,
            jobDescription: result.jobDescription

        });
    }).catch((reason) => {
        logger.info(reason);
        res.status(500).send({
            message: "check nodejs logs"
        });
    });
};

module.exports = {
    createDocument,
    getDocumentById,
    getAllDocuments,
    updateDocument,
    deleteDocument

};

