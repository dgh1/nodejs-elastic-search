const elasticsearch = require('elasticsearch');
const logger = require('./logger');

const client = new elasticsearch.Client({
    host: 'localhost:9200'
    //log: 'trace'
});

console.log("\n- elastic connected to http://localhost:9200");
logger.info("- elastic connected to http://localhost:9200");

const getElasticClient = () => {
    return client;
};


module.exports = {
    getElasticClient
};



