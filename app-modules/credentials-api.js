const logger = require('./logger');
const models = require('./mongoose-models');

const bcrypt = require('bcrypt');
const saltRounds = 10;

/**
 * Register a new user
 *
 * 1. validate the user does not already exist
 * 2. hash the password
 * 3. create user in DB
 *
 * @param req
 * @param res
 */
const registerUser = (req, res) => {
    logger.info("Mongoose - registerUser() ");

    let data = req.body;
    let entry = new models.credentialsModel({
        userName: data.userName,
        password: data.password,
        email: ""
    });
    let query = {
        userName: data.userName
    };

    new Promise(function (resolve, reject) {
        // verify user name does not already exist
        models.credentialsModel.findOne(query, (err, result) => {
            if (result !== null) {
                reject({
                    error: "User name already exist"
                });
            }
            resolve(result);
        });

    }).then(function (/*result*/) {
        // hash the password
        return new Promise((resolve, reject) => {
            bcrypt.hash(data.password, saltRounds, function (err, hash) {
                if (err === undefined) {
                    resolve(hash);
                }
                else {
                    reject(err);
                }
            });
        });

    }).then(function (hash) {
        // add the new user
        entry.password = hash;
        return new Promise((resolve, reject) => {
            entry.save(function (err, result) {
                if (err !== null) {
                    reject({
                        error: "failed to register user"
                    });
                }
                resolve(result);
            });
        });

    }).then(function (result) {
        res.status(200).send({
            status: "OK",
            document: result
        });

    }).catch((reason) => {
        logger.info(reason);
        let rsp = {
            reason: reason.error
        };
        res.status(400).send(rsp);
    });

};

/**
 * Login user
 *
 * 1. validate user name exist in DB
 * 2. validate the password
 *
 * @param req
 * @param res
 */
const loginUser = (req, res) => {
    logger.info("Mongoose - loginUser() ");

    let data = req.body;
    let userName = data.userName;
    let password = data.password;
    let query = {
        userName: userName
    };

    new Promise(function (resolve, reject) {
        // verify user name exist
        models.credentialsModel.findOne(query, (err, result) => {
            if (result === null) {
                reject({
                    error: 'user name ' + userName + ' does exist'
                });
            }
            resolve(result);
        });

    }).then(function (result) {
        return new Promise((resolve, reject) => {
            // validate password against hashed version from DB
            bcrypt.compare(password, result.password).then(function (res) {
                if (res !== true) {
                    reject({
                        error: 'invalid password'
                    });
                }
                resolve(res);
            });
        });

    }).then(function (/*result*/) {
        // update the login status
        return new Promise((resolve, reject) => {
            data.loggedIn = true;
            data.logInOutTime = new Date();

            models.personModel.findOneAndUpdate({"userName": data.userName}, data, function (err, result) {
                if (err !== null) {
                    reject({
                        error: "failed to update document ID: " + data.userName
                    });
                }
                resolve(result);

            });
        });

    }).then(function (result) {
        res.status(200).send({
            status: "OK",
            reason: result
        });

    }).catch((reason) => {
        logger.info(reason);
        res.status(406).send({
            reason: reason
        });
    });
};

const logoutUser = (req, res) => {
    logger.info("Mongoose - logoutUser() ");

    let data = req.body;
    let userName = data.userName;
    let query = {
        userName: userName
    };

    new Promise(function (resolve, reject) {
        // verify user name exist
        models.credentialsModel.findOne(query, (err, result) => {
            if (result === null) {
                reject({
                    error: 'user name ' + userName + ' does exist'
                });
            }
            resolve(result);
        });

    }).then(function (/*result*/) {
        // update the login status
        return new Promise((resolve, reject) => {
            data.loggedIn = false;
            data.logInOutTime = new Date();

            models.personModel.findOneAndUpdate({"userName": data.userName}, data, function (err, result) {
                if (err !== null) {
                    reject({
                        error: "failed to update document ID: " + data.userName
                    });
                }
                resolve(result);
            });
        });

    }).then(function (result) {
        res.status(200).send({
            status: "OK",
            reason: result
        });

    }).catch((reason) => {
        logger.info(reason);
        res.status(406).send({
            reason: reason
        });
    });
};

const getUsers = (req, res) => {
    logger.info("Mongoose - getAllDocuments() ");

    let promise = new Promise(function (resolve, reject) {
        models.credentialsModel.find(function (err, users) {
            if (err !== null) {
                reject({
                    error: "failed to get all uses"
                });
            }
            resolve(users);
        });
    });

    promise.then(function (users) {
        let allUsers = [];

        for (let user of users) {
            allUsers.push({
                id: user._id,
                userName: user.userName,
                email: user.email,
                password: user.password,
            });
        }

        res.status(200);
        res.send(allUsers);

    }).catch((reason) => {
        logger.info(reason);
        res.status(500).send({
            message: "check nodejs logs"
        });
    });
};

const deleteUser = (req, res) => {
    logger.info("Mongoose - deleteDocument() ");

    let params = JSON.parse(JSON.stringify(req.params));

    let promise = new Promise(function (resolve, reject) {
        models.credentialsModel.findOneAndDelete({"_id": params.id}, function (err, result) {
            if (err !== null) {
                reject({
                    error: "failed to delete userID: " + params.id
                });
            }
            resolve(result);

        });
    });

    promise.then(function (/*result*/) {
        res.status(200);
        res.send({
            status: "OK"
        });

    }).catch((reason) => {
        logger.info(reason);
        res.status(500).send({
            message: "check nodejs logs"
        });
    });
};


module.exports = {
    registerUser,
    loginUser,
    logoutUser,
    getUsers,
    deleteUser

};

