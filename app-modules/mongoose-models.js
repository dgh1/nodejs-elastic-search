const mongoose = require('mongoose');

const personSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    jobDescription: String
});

const credentialsSchema = new mongoose.Schema({
    userName: String,
    password: String,
    loggedIn: Boolean,
    logInOutTime: Date,
    email:  String
});


module.exports = {
    personModel: mongoose.model('Person', personSchema),
    credentialsModel: mongoose.model('Credentials', credentialsSchema)
};



